var Standup = require('../models/standup.server.model');

var listNotes = function(req,res) {
    var query = Standup.find();
    query.sort({ createdOn : 'desc'}).exec(function (err, results){
        
        if(err)
        console.error(err);

        results.forEach(function(result){
            
        })

        res.send(JSON.stringify(results));
    });
}

var create = function(req,res){
    var entry = new Standup({
        memberName : req.body.memberName,
        project : req.body.project,
        workYesterday : req.body.workYesterday,
        workToday : req.body.workToday,
        impediment : req.body.impediment
    });

    entry.save();

    res.redirect(301,'/');

};

var getNote = function (req,res){
    res.render('newnote', {title : 'standup notes page'});
}

module.exports = {
    create : create,
    getNote : getNote,
    listNotes : listNotes
}