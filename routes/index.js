var express = require('express');
var router = express.Router();
var standupCntrl = require('../controllers/standup.server.controller');

/* GET home page. */
router.get('/', function(req, res, next) {
  return standupCntrl.listNotes(req,res);
});

router.get('/newnote', function(req,res){
  return standupCntrl.getNote(req,res);
});

router.post('/newnote', function (req, res) {
  return standupCntrl.create(req, res);
});
module.exports = router;
